const express = require('express')
const app = express();
const { networkInterfaces } = require('os');
const port = process.env.PORT || 3000;

const network = networkInterfaces();
const date = new Date();
const ipAddress = {};

for (const name of Object.keys(network)) {
    for (const net of network[name]) { 
        const familyV4Value = typeof net.family === 'string' ? 'IPv4' : 4
        if (net.family === familyV4Value && !net.internal) {
            if (!ipAddress[name]) {
                ipAddress[name] = [];
            }
            ipAddress[name].push(net.address);
        }
    }
}

const response = {
    date: date.toLocaleString(),
    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone, 
    ipAddresses: ipAddress
}

app.get('/', (req, res) => {
  res.status(200).json({...response, hostname: req.headers.host});
})

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
})